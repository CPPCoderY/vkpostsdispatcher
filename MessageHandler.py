from EntryHandler import EntryHandler
import socket


class MessageHandler:
    def __init__(self, sock, group, admins):
        self.socket = sock
        self.vk_group = group
        self.entry_handler = EntryHandler(group, admins)
        self.admins_list = admins

    def handle_message(self, data):
        message = data['object']
        text = message['body']
        author = message['user_id']
        print('Message \'{}\' got, author {}'.format(text, author))
        if text == '' and message['attachments'] != '':
            link = 'DnP ' + message['attachments'][0]['doc']['preview']['audio_msg']['link_ogg']
            try:
                self.vk_group.send_message('Отправляю записанный звук...', author)
                self.socket.send(link.encode('utf-8'))
                self.vk_group.send_message(self.socket.recv(1024).decode('utf-8'), author)
            except socket.error as e:
                self.reset_connection()
                self.vk_group.send_message('Ошибка: {0}, соединение сброшено'.format(e), author)
        
        if 'привет' in text.lower():
            self.vk_group.send_message('Привет, я бот, конролирую гусей и домашние задания, с этого момента'
                                       'вы будете получать сообщения с домашним заданием и'
                                       ' важной информацией из этой группы.', author)
        elif 'list' in text.lower() and str(author) in self.admins_list:
            try:
                self.vk_group.send_message('Отправляю запрос...', author)
                self.socket.send('GETLISTS'.encode('utf-8'))
                self.vk_group.send_message(self.socket.recv(1024).decode('utf-8'), author)
            except socket.error as e:
                self.reset_connection()
                self.vk_group.send_message('Ошибка: {0}, соединение сброшено'.format(e), author)
        elif 'play' in text.lower() and str(author) in self.admins_list:
            try:
                self.vk_group.send_message('Отправляю запрос...', author)
                self.socket.send(text.encode('utf-8'))
                self.vk_group.send_message(self.socket.recv(1024).decode('utf-8'), author)
            except socket.error as e:
                self.reset_connection()
                self.vk_group.send_message('Ошибка: {0}, соединение сброшено'.format(e), author)
        elif 'stop' in text.lower() and str(author) in self.admins_list:
            try:
                self.vk_group.send_message('Отправляю запрос...', author)
                self.socket.send(text.encode('utf-8'))
                self.vk_group.send_message(self.socket.recv(1024).decode('utf-8'), author)
            except socket.error as e:
                self.reset_connection()
                self.vk_group.send_message('Ошибка: {0}, соединение сброшено'.format(e), author)
        elif 'e_stop' == text.lower() and str(author) in self.admins_list:
            try:
                self.vk_group.send_message('Отправляю запрос на блокировку!', author)
                self.socket.send(text.encode('utf-8'))
                self.vk_group.send_message(self.socket.recv(1024).decode('utf-8'), author)
            except socket.error as e:
                self.reset_connection()
                self.vk_group.send_message('Ошибка: {0}, соединение сброшено'.format(e), author)

    def set_socket(self, sock):
        self.socket = sock
        self.socket.settimeout(10)

    def reset_connection(self):
        self.socket.shutdown()
        self.socket.close()
