# from datetime import datetime


class EntryHandler:
    def __init__(self, group, admins):
        self.group = group
        # self.gooses = []
        # self.load_data()
        self.admins_list = admins
        print(group.id)
        self.admins_list.append(str(group.id))

    def handle(self, post):
        post = post['object']
        print('Handling post :', post['text'])
        # if '░' in post['text'] or '▀' in post['text'] or '▒' in post['text']:
        #     print('Goose found')
        #     if self.check_goose():
        #         print('Goose applied')
        #         self.group.comment_post(post['id'], 'Гусь подтвержден ботом')
        #         self.write_data('goose', post['id'], post['from_id'], datetime.now().timestamp(), 'APPLY')
        #     else:
        #         print('Goose deleted')
        #         self.group.delete_post(post['id'])
        #         self.group.send_message('Ваш запись №{}, первые 20 символов: {}, была удален ботом, '
        #                                 'возможно вы перепутали время, или разместили второго гуся за день.'
        #                                 'Если вы не согласны, обратитесь к Андрею в течении 5 часов, по истечению'
        #                                 'этого времени воостановить запись будет невозможно!', post['from_id'])
        #         self.write_data('goose', post['id'], post['from_id'], datetime.now().timestamp(), 'DELETE')
        if '[дз]' in post['text'].lower() and str(post['from_id']) in self.admins_list: 
            self.group.send_notifications('Размещено новое домашнее задание!', post['id'], post['from_id'])
            self.group.comment_post(post['id'], 'Рассылка выполнена')
        elif (('[ИНФОРМАЦИЯ]' in post['text']) or ('[INFO]' in post['text']) or ('[ИНФО]' in post['text'])) \
                and str(post['from_id']) in self.admins_list:
            self.group.send_notifications('Важная информация!', post['id'], post['from_id'])
            self.group.comment_post(post['id'], 'Рассылка выполнена')
        print('Handling end')

    # def check_goose(self):
    #     if not self.gooses and datetime.fromtimestamp(self.gooses[-1]['time']).day == datetime.now().day:
    #         return False
    #     else:
    #         weekday = datetime.now().weekday()
    #         goose_days = [0, 2, 4]
    #         return weekday in goose_days
    #
    # def load_data(self):
    #     try:
    #         file = open('dat.gc', 'r')
    #         for line in file:
    #             line = line.split(' ')
    #             element = {'type': line[0], 'id': line[1], 'author': line[2], 'time': line[3], 'action': line[4]}
    #             if element['type'] == 'goose' and element['action'] == 'APPLY':
    #                 self.gooses.append(element)
    #         file.close()
    #     except FileNotFoundError:
    #         print("File do not exists")
    #
    # def write_data(self, post_type, post_id, author, action, unix_time_stamp):
    #     if post_type == 'goose' and action != 'DELETE':
    #         element = {'type': post_type, 'id': post_id, 'author': author, 'time': unix_time_stamp, 'action': action}
    #         self.gooses.append(element)
    #     file = open('dat.gc', 'a')
    #     file.write('{0} {1} {2} {3} {4} \n'.format(post_type, post_id, author, unix_time_stamp, action))
    #     file.close()
