#! /usr/bin/python3

import vk
from flask import *
from group import Group
from MessageHandler import MessageHandler
from EntryHandler import EntryHandler
import socket
from threading import Thread
import traceback

#Токены и ключи по понятным причинам скрыты

token = 'XXXX'
shl_group_token = 'XXXX'
class_group_token = 'XXXX'
ad_group_token = 'XXXX'
mom_group_token = 'XXXX'

app = Flask(__name__)

shl_id = 146769467
class_id = 134408202
ad_id = 91726326
mom_id = 00000000

last_msg_id = 0

grids_list = None
handler = None

PORT = 4646
IP = '0.0.0.0'

wall_api = vk.API(vk.Session(access_token=token))
class_group_api = vk.API(vk.Session(access_token=class_group_token))
shl_group_api = vk.API(vk.Session(access_token=shl_group_token))
ad_group_api = vk.API(vk.Session(access_token=ad_group_token))
mom_group_api = vk.API(vk.Session(access_token=mom_group_token))

mom_group = Group(mom_id, wall_api, mom_group_api)
class_group = Group(class_id, wall_api, class_group_api)
shl_group = Group(shl_id, wall_api, shl_group_api)
ad_group = Group(ad_id,wall_api, ad_group_api)

#          Богданов      Белякова     Ирина             Ковальчук     Богданова
admins = ['295124418',  '96144545', '178369227',      '141283423',    '1707629']


def tcp_loop():
    while True:
        client, addr = sock.accept()
        handler.set_socket(client)
        print("Client {} connected".format(addr))


@app.route('/')
def hello():
    return render_template("index.html")

@app.errorhandler(500)
def internal_error(exception):
    trace = traceback.format_exc()
    print(trace)  
    return 'ok'
    

@app.route('/', methods=['POST'])
def data_got():
    data = json.loads(request.data)
    global admins, sock, shl_group, class_group, handler, last_msg_id, class_entry_handler, mom_handler, mom_group
    print('Data got:', data)
    if 'type' not in data.keys():
        print("Error")
        return "Error"
    elif data['type'] == 'confirmation' and data['group_id'] == class_id:
        print('Confirmation {}'.format(data['group_id']))
        return '644c4da4'
    elif data['type'] == 'confirmation' and data['group_id'] == shl_id:
        print('Confirmation {}'.format(data['group_id']))
        return 'd6293286'
    elif data['type'] == 'confirmation' and data['group_id'] == ad_id:
        print('Confirmation {}'.format(data['group_id']))
        return 'b7b0d8db'
    elif data['type'] == 'confirmation' and data['group_id'] == mom_id:
        print('Confirmation {}'.format(data['group_id']))
        return '53ab697d'
    elif data['type'] == 'message_new' and data['group_id'] == shl_id:
        current_id = int(data['object']['id'])
        if last_msg_id < current_id:
            handler.handle_message(data)
            last_msg_id = current_id
        print('Message handled')
        return 'ok'
    elif data['type'] == 'wall_post_new' and data['group_id'] == ad_id:
        ad_entry_handler.handle(data)
        print('Post handled')
        return 'ok'
    elif data['type'] == 'wall_post_new' and data['group_id'] == mom_id:
        mom_handler.handle(data)
        print('Mom post handled')
        return 'ok'
    elif data['type'] == 'wall_post_new' and data['group_id'] == shl_id:
        entry_handler.handle(data)
        print('Post handled')
        return 'ok'
    elif data['type'] == 'wall_post_new' and data['group_id'] == class_id:
        class_entry_handler.handle(data)
        print('Post handled')
        return 'ok'



if __name__ == '__main__':
    print('Starting server...')
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((IP, 4646))
    sock.listen(0)

    handler = MessageHandler(sock, shl_group, admins)
    entry_handler = EntryHandler(shl_group, admins)
    class_entry_handler = EntryHandler(class_group, admins)
    ad_entry_handler = EntryHandler(ad_group, admins)
    mom_handler = EntryHandler(mom_group, admins)    

    server = Thread(target=tcp_loop)
    server.start()

    print('Thread started')

    app.run(host="0.0.0.0", port=88, debug=False)

    print('Application stopped')
