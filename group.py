import vk


class Group:
    def __init__(self, group_id, wall_api, group_api):
        self.id = -group_id
        self.wall_api = wall_api
        self.group_api = group_api

    def post_message(self, message):
        self.wall_api.wall.post(owner_id=self.id, message=message, from_group=1)

    def get_entries(self):
        return self.wall_api.wall.get(owner_id=self.id, count=5)

    def delete_post(self, post_id):
        self.wall_api.wall.delete(owner_id=self.id, post_id=post_id)
        print("Deleted post№{}".format(post_id))

    def comment_post(self, post_id, comment):
        self.wall_api.wall.createComment(owner_id=self.id, post_id=post_id, from_group=-self.id, message=comment)

    def unpin(self, post_id):
        self.wall_api.wall.unpin(owner_id=self.id, post_id=post_id)
        print("Unpinned post№{0}".format(post_id))

    def pin(self, post_id):
        self.wall_api.wall.pin(owner_id=self.id, post_id=post_id)
        print("Pinned post№{0}".format(post_id))

    def get_dialogs(self):
        print('Getting dialogs list...')
        dialogs = self.group_api.messages.getDialogs(count=100)
        return dialogs['items']

    def send_notifications(self, message, post_id, author):
        users = []
        for entry in self.get_dialogs():
            users.append(entry['message']['user_id'])
        print('Список для отправки: ', users)
        try:
            users.remove(str(self.id))
            print('Removed group id from users list')
        except ValueError:
            print('Group id in users list not found')
        try:
            result = self.group_api.messages.send(user_ids=users, message=message, attachment="wall{0}_{1}".format(self.id, post_id))
            print(result)
        except vk.exceptions.VkException as e:
            print(e)
        try:
            filename = 'clients_{}.list'.format(-self.id)
            file = open(filename, 'w')
            for user in users:
                file.write(str(user) + '\n')
            file.close()
        except Exception as e:
            print(e)


    def send_message(self, message, user_id):
        self.group_api.messages.send(user_id=user_id, message=message)
        print("Sent message to:{0}".format(user_id))

    # def get_name(self, user_id):
    #     user = self.wall_api.users.get(user_ids=user_id)
    #     return user['first_name'] + ' ' + user['last_name']

